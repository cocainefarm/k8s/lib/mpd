{
  _config+:: {
    mpd: {
      name: "mpd",
      image: {
        repo: "kube.cat/cocainefarm/mpd",
        tag: "0.22.3"
      },
      config: {
        music_dir: "/music",
        playlist_dir: "/var/lib/mpd/playlists",
        db_file: "/var/lib/mpd/database",
        port: 6600,
        log_level: "verbose",
        log_file: "/dev/stdout",
      },
      config_files: {}
    },
  },

  local k = import "ksonnet-util/kausal.libsonnet",
  local statefulset = k.apps.v1.statefulSet,
  local container = k.core.v1.container,
  local env = k.core.v1.envVar,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,

  local withEnv(name, value) = container.withEnv(
    env.new(name=name, value=value)),
  
  mpd: {
    deployment: statefulset.new(
      name=$._config.mpd.name
      , replicas=1
      , containers=[
        container.new(
          "mpd"
          , $._config.mpd.image.repo + ":" + $._config.mpd.image.tag
        ) + container.withPorts([port.new("mpd", $._config.mpd.config.port)])
        + container.withPorts([port.new("web", 8080)])
        + container.withEnvMap({
          "MPD_MUSIC_DIR": $._config.mpd.config.music_dir,
          "MPD_PLAYLIST_DIR": $._config.mpd.config.playlist_dir,
          "MPD_DB_FILE": $._config.mpd.config.db_file,
          "MPD_PORT": std.toString($._config.mpd.config.port),
          "MPD_LOG_LEVEL": $._config.mpd.config.log_level,
          "MPD_LOG_FILE": $._config.mpd.config.log_file,
        })
      ]
    ) +
    k.util.configMapVolumeMount($.mpd.configmap, "/config")
    + statefulset.spec.withServiceName($.mpd.service.metadata.name),
    service: k.util.serviceFor(self.deployment) + service.spec.withClusterIP("None"),
    configmap: k.core.v1.configMap.new("%s-config" % $._config.mpd.name, $._config.mpd.config_files),
  }
}

